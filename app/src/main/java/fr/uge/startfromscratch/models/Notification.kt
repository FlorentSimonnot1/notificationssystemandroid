package fr.uge.startfromscratch.models

import com.google.gson.GsonBuilder

/**
 * [Notification] represents a notification model
 */
data class Notification(
    val message: String,
    val type: String
) {

    companion object {
        /**
         * Creates a Notification object from api
         * @param payload - the api json result
         * @return a [Notification] object
         */
        fun fromApi(payload: String): Notification =
            GsonBuilder()
                .create()
                .fromJson(payload, Notification::class.java)
    }

}
