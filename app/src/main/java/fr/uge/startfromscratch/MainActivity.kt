package fr.uge.startfromscratch

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import fr.uge.startfromscratch.services.NotificationService

open class MainActivity : AppCompatActivity() {
    private lateinit var button: Button
    private lateinit var startButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startButton = findViewById(R.id.start_service)
        button = findViewById(R.id.stop_service)

        startButton.setOnClickListener { startService() }
        button.setOnClickListener { stopService() }
    }

    private fun startService()  {
        val myServiceIntent = Intent(this, NotificationService::class.java)
        myServiceIntent.putExtra(NotificationService.USER_KEY, "umZ2KQfKkDi3a3xhY5rmXRVbVT2mPL0t")
        myServiceIntent.action = NotificationService.START_SERVICE
        ContextCompat.startForegroundService(this, myServiceIntent)
    }


    private fun stopService() {
        val myServiceIntent = Intent(this, NotificationService::class.java)
        myServiceIntent.action = NotificationService.STOP_SERVICE
        ContextCompat.startForegroundService(this, myServiceIntent)
    }
}