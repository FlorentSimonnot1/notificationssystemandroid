package fr.uge.startfromscratch.services

import android.app.*
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import fr.uge.startfromscratch.MainActivity
import fr.uge.startfromscratch.R
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import ua.naiksoftware.stomp.Stomp
import ua.naiksoftware.stomp.StompClient
import ua.naiksoftware.stomp.dto.LifecycleEvent

/**
 * [NotificationService] provide a foreground service which display notifications.
 * Use a STOMP web socket to catch notifications.
 */
class NotificationService: Service() {
    private var isServiceRunning = false
    private var instance: NotificationService? = null
    private var compositeDisposable: CompositeDisposable? = null
    private var mStompClient: StompClient? = null
    private var mRestPingDisposable: Disposable? = null

    companion object {
        private const val NOTIFICATION_ID = 1
        private const val NOTIFICATION_CHANNEL = "Notification_Channel"
        private const val BASE_URL_LOCALHOST = "<MY_WEB_SOCKET_URL>"
        private const val BROKER_POINT = "<MY_BROKER_POINT>"
        private const val TAG = "Notification Service"

        /**
         * Start Service Action
         */
        const val START_SERVICE = "START_SERVICE"
        /**
         * Stop Service Action
         */
        const val STOP_SERVICE = "STOP_SERVICE"

        /**
         * User notification key
         */
        const val USER_KEY = "USER_KEY"
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name: CharSequence = "Notifications channel"
            val description = "Channel for notifications"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(NOTIFICATION_CHANNEL, name, importance)
            channel.description = description
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    private fun createNotification(text: String, highPriority: Boolean): Notification {
        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.drawable.ic_bell)
            .setContentTitle("Test notification")
            .setContentText(text)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)

        if (highPriority) {
            builder.priority = NotificationCompat.PRIORITY_HIGH
            builder.setDefaults(Notification.DEFAULT_VIBRATE)
        }

        val resultIntent = Intent(this, MainActivity::class.java)
        resultIntent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP

        builder.setContentIntent(PendingIntent.getActivity(this, 0, resultIntent, 0))
        return builder.build()
    }

    override fun onBind(p0: Intent?): IBinder = throw UnsupportedOperationException("Not implemented")

    override fun onCreate() {
        super.onCreate()
        instance = this
        createNotificationChannel()
        mStompClient = Stomp.over(Stomp.ConnectionProvider.OKHTTP, BASE_URL_LOCALHOST)
        resetSubscriptions()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent == null) {
            return START_STICKY_COMPATIBILITY
        }

        val key = intent.getStringExtra(USER_KEY)

        when(intent.action) {
            START_SERVICE -> startService(key)
            STOP_SERVICE -> stopService()
            else -> { Log.w("Notifications Service", "Unknown action") }
        }

        return START_NOT_STICKY
    }

    private fun connectStomp(notificationManager: NotificationManager, key: String?) {
        mStompClient?.withClientHeartbeat(1000)?.withServerHeartbeat(1000)
        resetSubscriptions()

        val dispLifecycle: Disposable = mStompClient!!.lifecycle()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { lifecycleEvent ->
                when (lifecycleEvent.type) {
                    LifecycleEvent.Type.OPENED -> Log.d(TAG, "Connection success")
                    LifecycleEvent.Type.ERROR -> {
                        Log.e(TAG, "Stomp connection error", lifecycleEvent.exception)
                    }
                    LifecycleEvent.Type.CLOSED -> {
                        Log.d(TAG, "Connection close")
                        resetSubscriptions()
                    }
                    LifecycleEvent.Type.FAILED_SERVER_HEARTBEAT -> Log.d(TAG, "Connection fail")
                    else -> {}
                }
            }

        compositeDisposable!!.add(dispLifecycle)

        // Receive notifications from web socket
        val dispTopic: Disposable = mStompClient!!.topic("$BROKER_POINT/$key")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ topicMessage ->
                // Convert API response to a notification
                val notification = fr.uge.startfromscratch.models.Notification.fromApi(topicMessage.payload)
                notificationManager.notify((1..100000).random(), createNotification(notification.message, true))
            }) { throwable -> Log.e(TAG, "Error on subscribe topic", throwable) }
        compositeDisposable!!.add(dispTopic)
        mStompClient?.connect()
    }

    private fun resetSubscriptions() {
        if (compositeDisposable != null) {
            compositeDisposable!!.dispose()
        }
        compositeDisposable = CompositeDisposable()
    }

    private fun stopWebSocket() {
        mStompClient?.disconnect()
        if (mRestPingDisposable != null) mRestPingDisposable!!.dispose()
        if (compositeDisposable != null) compositeDisposable!!.dispose()
    }

    override fun onDestroy() {
        stopWebSocket()
        isServiceRunning = false
        isServiceRunning = false
        instance = null
        mStompClient = null
        mRestPingDisposable = null
        super.onDestroy()
    }

    // Start foreground service
    // Connect to the eb socket
    // Show notification
    private fun startService(key: String?) {
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        startForeground(NOTIFICATION_ID, createNotification("Service running...", true))
        connectStomp(notificationManager, key)
        isServiceRunning = true
    }

    // Stop web socket connection
    // Stop foreground service and remove the notification.
    // Stop the foreground service.
    private fun stopService() {
        if (isServiceRunning) {
            stopWebSocket()
            stopForeground(true);
            stopSelf()
            isServiceRunning = false
            instance = null
            mStompClient = null
            mRestPingDisposable = null
        }
    }

}